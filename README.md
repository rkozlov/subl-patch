# README #

Sublime Text Build 4126 & Sublime Merge Build 2068 license patch.

### How to use? ###

```
git clone https://codeberg.org/rkozlov/subl-patch.git
cd subl-patch
sudo chmod +x run.bin
sudo ./run.bin
```

### Where is my license? ###
In Sublime text press "Help" -> "Enter License"  
Write any text and press "Use License"  
Congratulation!

### I'm not a author! ###
Original git: https://github.com/CodigoCristo/sublimepatch